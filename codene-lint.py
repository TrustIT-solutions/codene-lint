#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @author Gabriel Lopez
# simple parser
import sys

def length(_length):
    def decorator(func):
        def wrapper(str):
            if len(str) != _length:
                raise Exception("Campo {} no tiene {} caracteres".format(func.__name__, _length))
            return func(str)

        return wrapper

    return decorator


def is_numeric(func):
    def wrapper(str):
        if not str.isdigit():
            raise Exception("Campo '{}' no es digito: {}".format(func.__name__,  repr(str)))
        return func(str)
    return wrapper

@length(6)
@is_numeric
def casa(str):
    pass

@length(8)
@is_numeric
def fecha(str):
    """fecha formateado como aaaammdd"""
    if int(str) == 0:
        raise Exception("Campo fecha invalido: " + repr(str))

@length(2)
@is_numeric
def situacion(str):
    if str not in ("02", "04", "05"):
        raise Exception("Campo situacion incorrecto")

@length(1)
def usuario(str):
    if str not in ("S", "G"):
        raise Exception("Campo usuario incorrecto")

@length(1)
def tarjeta(str):
    if str not in ("T", "A", " "):
        raise Exception("Campo tarjeta incorrecto")

@length(1)
def refinanciacion(str):
    if str not in ("S", "N", " "):
        raise Exception("Campo refinancion incorrecto")

@length(9)
@is_numeric
def deuda(str):
    pass

@length(1)
def persona(str):
    if str not in ("F", "J"):
        raise Exception("Campo tipo persona incorrecto")

@length(4)
def tipo_doc(str):
    if str not in ("DNI ", "LE  ", "LC  ", "CI  ", "CUIT", "CUIL", "PAS ", "OTRO"):
        raise Exception("Campo tipo_doc incorrecto: '{}'".format(str))

@length(11)
@is_numeric
def doc(str):
    if int(str) == 0:
        raise Exception("Campo Documento invalido")

@length(50)
def nombre(str):
    if str[0] == " ":
        raise Exception("Campo nombre invalido")

@length(1)
def sexo(str):
    if str not in ("M", "F"):
        raise Exception("Campo sexo invalido: " + repr(str))

@length(1)
def civil(str):
    if str not in ("S", "C", "V", "E", "D", "P", "N", " "):
        raise Exception("Campo estado civil invalido")


@length(8)
def fechaOpt(str):
    if str != " " * 8 and not str.isdigit():
        raise Exception("Campo no es Fecha")


@length(3)
@is_numeric
def nacionalidad(str):
    pass

@length(50)
def telefonos(str):
    pass


@length(70)
def domicilio(str):
    if str == " " * 70:
        raise Exception("Campo domicilio Vacio")
    if str[0] == " ":
        raise Exception("Campo domicilio no alineado")

@length(3)
@is_numeric
def provincia(str):
    pass

@length(6)
@is_numeric
def localidad(str):
    pass


@length(250)
def referencia(str):
    pass

@length(50)
def catastro(str):
    pass
@length(255)
def actividad(str):
    pass


print("INFO: Abriendo archivo", sys.argv[1])
file = open(sys.argv[1], encoding="iso-8859-1")
content = file.read()
lines = content.split("\n")
print("INFO: Registros: {}".format(len(lines)))

for i in range(len(lines)):
    line = lines[i]
    if len(line) != 849:
        print("ERROR: linea {} tiene {} y debe ser 849".format(i +1, len(line)))
        continue

    if "\n" in line:
        print("ERROR: linea {} tiene un '\n'".format(i +1))
        continue

    try:
        casa(line[:6])
        fecha(line[6:14])
        situacion(line[14:16])
        usuario(line[16:17])
        tarjeta(line[17:18])
        refinanciacion(line[18:19])
        deuda(line[19:28])
        persona(line[28:29])
        tipo_doc(line[29:33])
        doc(line[33:44])
        nombre(line[44:94])
        sexo(line[94:95])
        civil(line[95:96])
        fechaOpt(line[96:104])

        nacionalidad(line[104:107])

        telefonos(line[107:157])
        domicilio(line[157:227])
        provincia(line[227:230])
        localidad(line[230:236])
        referencia(line[236:486])
        telefonos(line[486:536])
        fechaOpt(line[536:544])
        catastro(line[544:594])
        actividad(line[594:])

    except Exception as ex:
        print("Error: linea {}: {}".format(i + 1, (ex)))
