# Codene Lint

Verifica que el archivo que se envia a Codene este sintacticamente correcto.

Uso:

```
python3 codene-lint.py <archivo.txt>
```
